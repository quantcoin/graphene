## testnet

### Keys

Keys are generate using
```
$ ./cli_wallet --sugest-brain-key
brain private key: SHIPPO LAPPING TROPIC TOWEL ROUNCE NOWHAT DEODARA ROUPER LAWTER MODELER VITROUS BINNITE AGENDUM WARNEL BISQUE UTILE
private key: 5KgJq8q6n6hBcrrBqG32pQLTXVSf8b2BrEfWd1ri4ahhX2ApZ3J
public key: GPH6rF8A4DEK39hoFUvwL2idMZeGsqbnNmUBK2g8zyafxN38EN4x3
address: GPHFGqKY6NZdEAfq1rTWEwPZUNph2ZH74aWy
```

### witness_node
`./run_witness_node.py`

### cli_wallet
`./run_cli_wallet.py`

You can find details here: http://docs.bitshares.eu/testnet/4-cli-wallet.html

First login:
```
set_password supersecret
unlock supersecret
import_key by.bsuir.fksis.evm.node0 5KgJq8q6n6hBcrrBqG32pQLTXVSf8b2BrEfWd1ri4ahhX2ApZ3J
import_balance by.bsuir.fksis.evm.node0 ["5KgJq8q6n6hBcrrBqG32pQLTXVSf8b2BrEfWd1ri4ahhX2ApZ3J"] true
list_account_balances by.bsuir.fksis.evm.node0
```

Create account and transfer coins:
```
suggest_brain_key
create_account_with_brain_key "Brain ... key" "newaccount" by.bsuir.fksis.evm.node0 by.bsuir.fksis.evm.node0 0 true
transfer by.bsuir.fksis.evm.node0 newaccount 2000 CORE "here is some cash" true
```


### Comitee member
http://docs.bitshares.eu/testnet/5-committee.html