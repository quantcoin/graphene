#!/usr/bin/python
import subprocess
from os import path

subprocess.call(path.join(path.dirname(__file__), '..', 'programs', 'witness_node', 'witness_node') + " --enable-stale-production  --data-dir=. --genesis-timestamp=10", shell=True)
