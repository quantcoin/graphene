#!/bin/bash

CMD="docker run --workdir=$(pwd) -ti --rm -v $(pwd):$(pwd) denis4inet/graphene-builder"

case $1 in
    run-witness)
        ${CMD} bash -c "ninja witness_node && cd testnet && python2 ./run_witness_node.py"
    ;;
    *)
         ${CMD} bash
    ;;
esac