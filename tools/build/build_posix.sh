#!/bin/bash
JOBS=4

export BOOST_ROOT=$(readlink -f boost_root)

build_boost_from_scratch() {
    BOOST_URL=https://sourceforge.net/projects/boost/files/boost/1.60.0/boost_1_60_0.tar.bz2/download
    BOOST_PACKAGE=$(mktemp)
    BOOST_DIR=boost_1_60_0

    [ -d $BOOST_DIR ] || (wget $BOOST_URL -O $BOOST_PACKAGE && tar -xf $BOOST_PACKAGE); (rm -rf $BOOST_PACKAGE && exit 1)

    mkdir -p boost_root
    export BOOST_ROOT=$(readlink -f boost_root)

    cd $BOOST_DIR
    ./bootstrap.sh --prefix=$BOOST_ROOT --with-toolset=clang --with-python=/usr/bin/python2.7 || exit 4
    ./b2 -j$JOBS || exit 5
    ./b2 install
    cd ..
}

install_prebuilt_boost() {
    wget https://github.com/denis4net/boost/releases/download/boost-1.60.0/boost-1.60.0-clang-linux-x64.tar.bz2 -O boost_root.tar.gz
    mkdir -p boost_root
    export BOOST_ROOT=$(readlink -f boost_root)
    tar -xf boost_root.tar.gz -C $BOOST_ROOT
}


echo "Boost root: $BOOST_ROOT"
[ -d "$BOOST_ROOT" ] || install_prebuilt_boost

if [ "$BUILD_TYPE" == "debug" ]; then
    cmake -DGRAPHENE_EGENESIS_JSON=$(readlink -f testnet/genesis.json) -DBOOST_ROOT=$BOOST_ROOT -DCMAKE_CXX_COMPILER=$(which clang++) -DCMAKE_C_COMPILER=$(which clang) -GNinja -DCMAKE_BUILD_TYPE=Debug . || exit 6
    ninja witness_node cli_wallet $@
else
    cmake -DFULL_STATIC_BUILD=1 -DGRAPHENE_EGENESIS_JSON=$(readlink -f testnet/genesis.json) -DENABLE_INSTALLER=1 -DBOOST_ROOT=$BOOST_ROOT -DCMAKE_CXX_COMPILER=$(which clang++) -DCMAKE_C_COMPILER=$(which clang) -GNinja . || exit 6
    ninja witness_node cli_wallet $@ || exit 7
fi
